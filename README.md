Zadanie rekrutacyjne dla programisty aplikacji JavaScript
=========================================================

Utwórz stronę z formularzem, który posłuży do wyszukiwania podręczników szkolnych w księgarni Gdańskiego Wydawnictwa Oświatowego.
Formularz powinien zawierać jedno pole tekstowe do wpisania hasła i przycisk submit. 
Formularz powinien być walidowany (od 3 do 12 znaków).
Kod pobierze listę pozycji z API, które jest dostępne pod adresem:

    https://gwo.pl/booksApi/v1/search?query=historia

Każdą książkę pokaż w osobnej ramce i zaprezentuj dane:
* tytuł,
* autorzy,
* ISBN,
* numer dopuszczenia MEN,
* liczba stron,
* poziomy nauczania,
* przedmiot,
* rodzaj publikacji (podręcznik, zeszyt ćwiczeń, materiały dodatkowe),
* okładka.

Wyświetlone pozycje powinny być stronicowane (6 elementów na stronę). Należy zapewnić nawigację po kolejnych stronach wyników.

Dodatkowo wyświetl przycisk "Przejdź do księgarni", linkujący do konkretnej pozycji na stronie gwo.pl.

Pamiętaj, że przekazując parametry do API musisz je zakodować, czyli np. dla frazy "język polski" adres URL wygląda tak:

    https://gwo.pl/booksApi/v1/search?query=j%C4%99zyk%20polski

Będziemy przyznawać punkty za:
1. Poprawną strukturę HTML 5, CSS i JavaScript/Typescript, formatowanie kodu.
2. Użycie Angular 2+ lub Ember / React (preferowany Angular 2+).
3. Użycie Ngrx / Redux.
4. Obsługę błędów.
5. Testy.
6. Zastąpienie przycisku submit mechanizmem live search (wysyłanie żądań do API w trakcie wpisywania danych do formularza).

W repozytorium powinien znajdować się krótki opis instalacji i uruchomienia aplikacji.
